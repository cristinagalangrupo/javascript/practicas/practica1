var letras = ['T', 'R', 'W', 'A', 'G', 'M', 'Y', 'F', 'P', 'D', 'X', 'B', 'N', 'J', 'Z', 'S', 'Q', 'V', 'H', 'L', 'C', 'K', 'E', 'T'];
var numero = 0; // almacena el número del dni
var letra = 0; // almacena letra del dni
var posicion = 0; // almacena la posición de la letra en el array de letras(mod(n,23))
var l = 0;//almacena la letra correspondiente del array


numero = parseInt(prompt("Introduce el número del D.N.I."));

if (numero < 0 || numero > 99999999) { // Compruebo si el número introducido es válido
    alert("El número no es válido"); // NO VÁLIDO
} else { // SÍ VÁLIDO
    
    letra = prompt("Introduce letra del D.N.I."); // Leo letra
    
    letra = letra.toUpperCase(); // Paso a mayús. para comparar
    posicion = numero % 23;// Calulo el resto que coincide con la posición de la letra del array
    l = letras[posicion]; // almaceno letra


    if (l == letra) { // comparo
        alert("El número y la letra son correctos");
    } else {
        alert("La letra indicada no es correcta");
    }
}