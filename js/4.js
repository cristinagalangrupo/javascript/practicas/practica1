
/*  *********************************** APARTADO 1   ***********************************  */
var valores=[true,5,false,"hola","adios",2];
var longitud=0; //declaro una variable para almacenar la longitud de las palabras
var palabraLarga=0; // declaro una variable para almacenar la palabra más larga

for(i=0;i<valores.length;i++){ // recorro el array
	if (valores[i].length>longitud) {//si la longitud de la palabra es mayor que la variable longitud
		longitud=valores[i].length; // La variable longitud vale la longitud de la pabla
		palabraLarga=valores[i]; // almaceno la palabra
	}
}
console.log("La palabra más larga es: "+palabraLarga);

/*  *********************************** APARTADO 2   ***********************************  */
var numeros=[]; // almaceno los números del array
// no puedo utilizar isNaN porque almacena los valores lógicos como 0 y 1
// utilizo typeof para quedarme sólo con los de tipo number
for(i=0;i<valores.length;i++){
	if (typeof(valores[i])=='number') {
		numeros[i]=valores[i];
	}
}
console.log("array de números" + numeros);

// utilizo for in para recorrer las posiciones que contienen algo
var suma=0;
var resta=0;
var producto=1;
var division=1;
var potencia=0;

suma=numeros[1]+numeros[5];
resta=numeros[1]-numeros[5];
producto=numeros[1]*numeros[5];
division=numeros[1]/numeros[5];
potencia=Math.pow(numeros[1],numeros[5]);


console.log("suma: "+suma,"resta: "+resta, "multiplicacion: " + producto, "division: " + division, "Potencia: " + potencia);

